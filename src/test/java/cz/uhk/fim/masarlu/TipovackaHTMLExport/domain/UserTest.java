package cz.uhk.fim.masarlu.TipovackaHTMLExport.domain;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class UserTest {
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{new User(1, "MAFline", 5), "1. MAFline 5.0"}, {new User(null, "MAFline", 4.2f), "MAFline 4.2"}
		});
	}
	
	@Parameter
	public User input;
	
	@Parameter(value = 1)
	public String expected;
	
	@Test
	public void testToString() {
		assertEquals(expected, input.toString());
	}

}
