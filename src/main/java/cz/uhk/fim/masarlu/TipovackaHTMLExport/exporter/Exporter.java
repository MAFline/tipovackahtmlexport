package cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter;

import java.io.IOException;

public interface Exporter {
	boolean generateOutputToFile() throws IOException;
}
