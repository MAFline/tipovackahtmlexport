package cz.uhk.fim.masarlu.TipovackaHTMLExport.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.Exporter;
import cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.csvToHtmlExporter.FullResultsExporter;
import cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.csvToHtmlExporter.LastMatchExporter;

public class CsvFounder {
	public static final String TYPE_LAST = "last";
	public static final String TYPE_FULL = "full";

	public List<Exporter> getExporters() throws IOException {
		List<Exporter> exporters;

		List<String> csvFiles = findCsvFiles();
		if (csvFiles.isEmpty())
			throw new IOException("Žádné soubory k parsování");

		exporters = new ArrayList<>();

		for (String file : csvFiles) {
			Exporter exporter = getExporter(file);
			if (exporter != null)
				exporters.add(exporter);
		}

		return exporters;
	}

	private Exporter getExporter(String file) {
		Exporter exporter = null;
		try {
			String csvType = file.substring(file.lastIndexOf("_") + 1,
					file.length() - 4);
			switch (csvType) {
			case TYPE_LAST:
				exporter = new LastMatchExporter(file);
				break;
			case TYPE_FULL:
				exporter = new FullResultsExporter(file);
				break;
			default:
				break;
			}
		} catch (IndexOutOfBoundsException e) {
			// neni potreba zachytavat, csv soubor nemel pozadovany sufix
		}

		return exporter;
	}

	private List<String> findCsvFiles() {
		String currentDir = Paths.get("").toAbsolutePath().toString();
		File[] listOfFiles = new File(currentDir).listFiles();
		List<String> csvFiles = new ArrayList<>();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String file = listOfFiles[i].toString();
				try {
					if (file.substring(file.length() - 3).equals("csv")) {
						csvFiles.add(file);
					}
				} catch (IndexOutOfBoundsException e) {
					// neni potreba zachytavat, csv soubor nemel pozadovany
					// sufix
				}
			}
		}

		return csvFiles;
	}
}
