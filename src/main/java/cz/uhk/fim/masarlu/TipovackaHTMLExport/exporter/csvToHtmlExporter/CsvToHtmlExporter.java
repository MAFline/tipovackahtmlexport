package cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.csvToHtmlExporter;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.masarlu.TipovackaHTMLExport.domain.User;
import cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.Exporter;

public abstract class CsvToHtmlExporter implements Exporter {
	protected String file;
	
	public CsvToHtmlExporter(String file) {
		this.file = file;
	}
	
	public CsvToHtmlExporter() {
	}

	protected String createOutputFileName(String inputFile) {
		String outputFileName = inputFile.substring(0, inputFile.lastIndexOf("/") + 1)
				+ "output_"
				+ inputFile.substring(inputFile.lastIndexOf("/") + 1,
						file.lastIndexOf(".")) + ".txt";
		
		return outputFileName;
	}
	
	protected List<User> setPositionsToUsers(final List<User> rankUsers) {
		List<User> users = new ArrayList<>();
		users.add(rankUsers.get(0));
		for (int i = 1; i < rankUsers.size(); i++) {
			User currUser = rankUsers.get(i);
			if (currUser.getPoints() == rankUsers.get(i - 1).getPoints())
				currUser.setPosition(null);
			users.add(currUser);
		}
		return users;
	}
	
	protected String formatPosition(Integer position) {
		if (position == null) return "";
		return position.toString() + ".";
	}

	protected String formatPoints(float d) {
		if (d == (int) d)
			return String.format("%d", (int) d);
		else
			return String.format("%s", d);
	}
	
	protected String formatParticipation(String participation) {
		Float particip = Float.parseFloat(participation.substring(0, participation.length() - 1));
		return Float.toString(particip).replaceAll("\\.?0*$", "") + "%";
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
}
