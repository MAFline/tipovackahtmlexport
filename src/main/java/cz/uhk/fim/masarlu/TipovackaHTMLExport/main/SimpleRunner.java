package cz.uhk.fim.masarlu.TipovackaHTMLExport.main;

import java.io.IOException;
import java.util.List;

import cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.Exporter;
import cz.uhk.fim.masarlu.TipovackaHTMLExport.model.CsvFounder;

public class SimpleRunner implements Runner {
	private List<Exporter> exporters;
	private CsvFounder csvFounder;

	public void run(String[] args) {
		csvFounder = new CsvFounder();
		
		try {
			exporters = csvFounder.getExporters();
			if (!exporters.isEmpty()) {
				for (Exporter exporter : exporters) {
					 try {
						 exporter.generateOutputToFile();
					 } catch (IOException e) {
						 System.err.println(e.getMessage());
					 }
				}
			}
		} catch (IOException e) {
			System.err.println("Export se nezdařil.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
}
