package cz.uhk.fim.masarlu.TipovackaHTMLExport.domain;

public class User {
	private Integer position;
	private String name;
	private String participation;
	private float points;
	
	public User(Integer position, String name, String participation, float points) {
		this.position = position;
		this.name = name;
		this.participation = participation;
		this.points = points;
	}

	public User() {
	}
	
	public User(Integer position, String name, float points) {
		this.position = position;
		this.name = name;
		this.participation = null;
		this.points = points;
	}
	
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParticipation() {
		return participation;
	}

	public void setParticipation(String participation) {
		this.participation = participation;
	}

	public float getPoints() {
		return points;
	}

	public void setPoints(float points) {
		this.points = points;
	}
	
	@Override
	public String toString() {
		StringBuilder user = new StringBuilder();
		if (position != null) user.append(position + ". ");
		user.append(name + " " + points);
		return user.toString();
	}
	
}