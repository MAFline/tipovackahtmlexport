package cz.uhk.fim.masarlu.TipovackaHTMLExport.exporter.csvToHtmlExporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.masarlu.TipovackaHTMLExport.domain.User;

public class LastMatchExporter extends CsvToHtmlExporter {

	public LastMatchExporter(String file) {
		super(file);
	}

	@Override
	public boolean generateOutputToFile() throws IOException {
		List<User> rankUsers;

		rankUsers = parseCsv();

		rankUsers = setPositionsToUsers(rankUsers);

		String outputFileName = createOutputFileName(file);
		String relPathName = new File("").toURI().relativize(new File(outputFileName).toURI()).getPath();

		File outputFile = new File(outputFileName);
		if (outputFile.exists())
			throw new IOException("Soubor " + relPathName + " již existuje.");

		createHtmlOutput(outputFile, rankUsers);

		System.out.println("Vytvořen soubor " + relPathName);
		
		return true;
	}

	private void createHtmlOutput(File outputFile, List<User> rankUsers)
			throws IOException {
		PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
		writer.println("<table class=\"table table-bordered table-condensed table-striped\">");
		writer.println("\t<thead>");
		writer.println("\t\t<tr>");
		writer.println("\t\t\t<th scope=\"col\" style=\"width:30px\">Umístění</th>");
		writer.println("\t\t\t<th scope=\"col\">Uživatel</th>");
		writer.println("\t\t\t<th scope=\"col\" style=\"width:30px\">Body</th>");
		writer.println("\t\t</tr>");
		writer.println("\t</thead>");
		writer.println("\t<caption>TODO</caption>");
		writer.println("\t<tbody>");

		for (User user : rankUsers) {
			writer.println("\t\t<tr>");
			writer.println("\t\t\t<td style=\"width:30px\">"
					+ formatPosition(user.getPosition()) + "</td>");
			writer.println("\t\t\t<td>" + user.getName() + "</td>");
			writer.println("\t\t\t<td style=\"width:30px\">"
					+ formatPoints(user.getPoints()) + "</td>");
		}

		writer.println("\t</tbody>");
		writer.println("</table>");
		writer.close();
	}
	
	private List<User> parseCsv() throws IOException {
		List<User> rankUsers = new ArrayList<>();
		String line = "";
		String csvSplit = ";";
		BufferedReader br = new BufferedReader(new FileReader(file));
		Integer position = 1;
		while ((line = br.readLine()) != null) {
			if (";;".equals(line)) break;
			String[] rawUser = line.split(csvSplit);
			String name = rawUser[0];
			float points = Float.parseFloat(rawUser[1].replace(",", "."));

			User user = new User(position++, name, points);
			rankUsers.add(user);
		}
		br.close();
		
		if (rankUsers.isEmpty()) {
			String relPath = new File("").toURI().relativize(new File(file).toURI()).getPath();
			throw new IOException("Soubor " + relPath + " neobsahoval zadna data.");
		}

		return rankUsers;
	}

}
